import java.sql.*;
import java.io.FileInputStream;
import java.util.Properties;

public class Select {

    public static void main(String args[]) {      
        Connection con = null;
        try {
            DS bdd = new DS();

            con = bdd.getConnection();
        
            // execution de la requete
            String query = "select NOM,PRENOM,AGE from CLIENTS";
            PreparedStatement ps = con.prepareStatement( query );
            ResultSet rs = ps.executeQuery();
      
            System.out.println("Liste des clients:");
            while (rs.next()) {
                String n = rs.getString("nom");
                String p = rs.getString("prenom");
                int a = rs.getInt("age");
                System.out.println(n + " " + p + " " + a);
            }

            System.out.println("Ok.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // fermeture des espaces
                con.close();
            } catch (Exception e2) {}
        }
    }

}