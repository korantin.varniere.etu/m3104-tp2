import java.sql.*;
import java.io.FileInputStream;
import java.util.Properties;

public class Compter {

    public static void main(String args[]) {      
        Connection con = null;
        try {
            DS bdd = new DS();

            con = bdd.getConnection();
            
            // execution de la requete
            String query = "select Count(*) as res from CLIENTS";
            PreparedStatement ps = con.prepareStatement( query );
            ResultSet rs = ps.executeQuery();

            rs.next();
            System.out.println(rs.getInt("res"));

            System.out.println("Ok.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // fermeture des espaces
                con.close();
            } catch (Exception e2) {}
        }
    }

}