import java.sql.*;
import java.io.FileInputStream;
import java.util.Properties;

public class Create {

    public static void main(String args[]) {      
        Connection con = null;
        try {
            DS bdd = new DS();

            con = bdd.getConnection();

            // execution de la requete
            String query = "create table CLIENTS (NOM varchar(10), PRENOM varchar(10), AGE int)";
            PreparedStatement ps = con.prepareStatement( query );
            ps.executeUpdate();

            System.out.println("Ok.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // fermeture des espaces
                con.close();
            } catch (Exception e2) {}
        }
    }

}