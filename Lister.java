import java.sql.*;
import java.io.FileInputStream;
import java.util.Properties;

public class Lister {

    public static void main(String args[]) {      
        Connection con = null;
        try {
            DS bdd = new DS();

            con = bdd.getConnection();
        
            // execution de la requete
            String query = "select * from " + args[0];
            PreparedStatement ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();

            int nbCol = rs.getMetaData().getColumnCount();
      
            System.out.println("Table " + args[0] + ":");

            for (int i = 1; i <= nbCol; i++) {
                if (i > 1) System.out.print(",  ");
                System.out.print(rs.getMetaData().getColumnName(i));
            }
            
            System.out.println();

            while (rs.next()) {
                for (int i = 1; i <= nbCol; i++) {
                    if (i > 1) System.out.print(",  ");
                    String value = rs.getString(i);
                    System.out.print(value);
                }
                System.out.println();
            }

            System.out.println("Ok.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // fermeture des espaces
                con.close();
            } catch (Exception e2) {}
        }
    }

}