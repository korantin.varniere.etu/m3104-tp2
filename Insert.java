import java.sql.*;
import java.io.FileInputStream;
import java.util.Properties;

public class Insert {

    public static void main(String args[]) {      
        Connection con = null;
        try {
            DS bdd = new DS();

            con = bdd.getConnection();

            // execution de la requete
            for (int i = 1; i <= 1000; i++) {
                String query = "insert into CLIENTS values(?, ?, ?)";
                PreparedStatement ps = con.prepareStatement(query);
                ps.setString(1, "nom_" + i);
                ps.setString(2, "pnom_" + i);
                ps.setInt(3, i);
                ps.executeUpdate();
            }

            System.out.println("Ok.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // fermeture des espaces
                con.close();
            } catch (Exception e2) {}
        }
    }

}